<?php
namespace app\api\controller;

use think\Request; //处理参数类
use think\Controller; //控制器基础类
use think\Validate; //参数验证类

class Common extends Controller
{
  protected $request; // 用来处理参数
  protected $validater; //用来验证数据/参数
  protected $params; //过滤后符合要求的参数（不包含time和token）
  protected $rules = array(
    'User' => array(
      'login' => array(
        'user_name' => ['require', 'chsDash', 'max' => 20],
        'user_pwd' => 'require|length:32'
      ),
    ),
  );
  protected function _initialize()
  {
    /*********** 继承父类的构造方法 ***********/
    parent::_initialize();
    /*********** 获取接受到的参数 ***********/
    $this->request = Request::instance();
    /*********** 判断时间戳 ***********/
    // $this->check_time($this->request->only(['time']));
    /*********** 判断token ***********/
    // $this->check_token($this->request->param());
    /*********** 验证参数 ***********/
    $this->check_params($this->request->except(['time', 'token']));
  }
  /**
   * 验证时间戳是否超时
   *
   * @param array $arr 包含时间错的参数数组
   * @return json 检测结果
   */
  public function check_time($arr)
  {
    if (!isset($arr['time']) || intval($arr['time']) <= 1) { //判断时间戳是否存在
      $this->return_msg(400, '时间戳不正确');
    }
    if (time() - intval($arr['time']) > 60) { //判断时间戳是否超时：大于60s 
      $this->return_msg(400, '请求超时！');
    }
  }
  /**
   * api 数据返回
   *
   * @param int $code 结果码  200：正常、4**：数据问题、5**：服务器问题
   * @param string $msg 接口要返回的提示信息
   * @param array $data 接口要返回的数据
   * @return string 最终的json数据
   */
  public function return_msg($code, $msg = '', $data = [])
  {
    /*********** 组合数据 ***********/
    $return_data['code'] = $code;
    $return_data['msg'] = $msg;
    $return_data['data'] = $data;
    /*********** 返回信息并终止脚本 ***********/
    echo json_encode($return_data);
    die;
  }
  /**
   * 验证token，防止篡改数据
   *
   * @param array $arr 全部请求数据
   * @return json token验证结果
   */
  public function check_token($arr)
  {
    /*********** api传过来的token ***********/
    if (!isset($arr['token']) || empty($arr['token'])) {
      $this->return_msg(400, 'token不能为空');
    }
    $app_token = $arr['token']; //api传过来的token
    /*********** 服务器生成的token ***********/
    unset($arr['token']);
    $service_token = '';
    foreach ($arr as $key => $value) {
      $service_token .= md5($value); //每个元素MD5加密
    }
    $service_token = md5('api_' . $service_token . '_api'); //服务器即时生成的token
    /*********** 对比token，返回结果 ***********/
    if ($app_token !== $service_token) {
      $this->return_msg(400, 'token不正确');
    }
  }
  /**
   * 验证参数
   *
   * @param array $arr 除time和token外的所有参数
   * @return void
   */
  public function check_params($arr)
  {
    /*********** 获取参数的验证规则 ***********/
    $rule = $this->rules[$this->request->controller()][$this->request->action()];
    /*********** 验证参数并返回错误 ***********/
    $this->validater = new Validate($rule);
    if (!$this->validater->check($arr)) {
      $this->return_msg(400, $this->validater->getError());
    }
    /*********** 如果正常，通过验证 ***********/
    $this->params = $arr;
  }
}