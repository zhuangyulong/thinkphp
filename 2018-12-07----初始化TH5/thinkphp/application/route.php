<?php

use think\Route;

// api.tp5.com ===> www.tp5.com/index.php/api
Route::domain('api', 'api');
// api.tp5.com/user/2 ===> www.tp5.com/index.php/api/user/index/id/2
// Route::rule('user/:id', 'user/index');
// post api.tp5.com ===> user.php login()
Route::post('user', 'user/login');