﻿# Host: localhost  (Version: 5.5.53)
# Date: 2018-12-08 19:03:20
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "api_article"
#

DROP TABLE IF EXISTS `api_article`;
CREATE TABLE `api_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_uid` int(11) DEFAULT NULL COMMENT '用户id',
  `article_content` text COMMENT '内容',
  `article_ctime` int(11) DEFAULT NULL COMMENT '创建时间',
  `article_isdel` int(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='文章表';

#
# Data for table "api_article"
#

/*!40000 ALTER TABLE `api_article` DISABLE KEYS */;
INSERT INTO `api_article` VALUES (1,'11',1,'1',1544261596,0),(2,'22',1,NULL,1544261596,0),(3,'33',1,NULL,1544261600,0),(4,'11',2,NULL,1544261629,0),(5,'22',2,NULL,1544261631,0),(6,'33',2,NULL,1544261634,0),(7,'11',3,NULL,1544261638,0),(8,'22',3,NULL,1544261642,0),(9,'33',3,NULL,1544261645,0),(10,'44',1,NULL,1544261651,0),(11,'44',2,NULL,1544261654,0),(12,'44',3,NULL,1544261656,0);
/*!40000 ALTER TABLE `api_article` ENABLE KEYS */;

#
# Structure for table "api_user"
#

DROP TABLE IF EXISTS `api_user`;
CREATE TABLE `api_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_phone` char(11) DEFAULT NULL,
  `user_nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
  `user_email` varchar(255) DEFAULT NULL,
  `user_rtime` int(11) DEFAULT NULL COMMENT '注册时间',
  `user_pwd` char(32) DEFAULT NULL,
  `user_icon` varchar(255) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "api_user"
#

/*!40000 ALTER TABLE `api_user` DISABLE KEYS */;
INSERT INTO `api_user` VALUES (1,'13333333331','zyl1','295993090@qq.com',1544247108,'4297f44b13955235245b2497399d7a92','/uploads/20181208/b02ac26bfe1f145efea77e5bdc1a18ce.jpg'),(2,'13333333332','zyl2','29599390@qq.com',1544247108,'4297f44b13955235245b2497399d7a91',NULL),(3,'13333333333','zyl3','29599309@qq.com',1544247108,'4297f44b13955235245b2497399d7a91',NULL);
/*!40000 ALTER TABLE `api_user` ENABLE KEYS */;
