# 1. 获取验证码
> `get` api.tp5.com/code

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|username|string|`必需`|无|手机或者邮箱|
|is_exist|int|`必需`|无|用户名是否应该存在（1：是，0：否）|

```javascript
{
    "code": 200,
    "msg" : "手机验证码已经发送成功，每天可以发送5次，请在1分钟内验证",
    "data": []
}
```

# 2. 用户注册
> `post` api.tp5.com/user/register

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_name|string|`必需`|无|手机或者邮箱|
|user_pwd|string|`必需`|无|md5加密的用户密码|
|code|int|`必需`|无|用户收到的验证码|

```javascript
{
    "code": 200,
    "msg" : "注册成功",
    "data": 1 //插入数据的行数
}
```

# 3. 用户登录
> `post` api.tp5.com/user/login

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_name|string|`必需`|无|手机或者邮箱|
|user_pwd|string|`必需`|无|md5加密的用户密码|

```javascript
{
    "code": 200,
    "msg": "登录成功！",
    "date": {
        "user_id": 1,
        "user_name": "18883373591",
        "user_phone": "18883373591",
        "user_email": null,
        "user_rtime": 12132123
    }
}
```

# 4. 用户上传头像
> `post` api.tp5.com/user/icon

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|user_icon|file|`必需`|无|用户上传的头像|

```javascript
{
    "code": 200,
    "msg": "头像上传成功！",
    "date": "/uploads/20180301/e97f3bc3a4209fe1a7f252be786822ea.jpg"
}
```

# 5. 用户修改密码
> `post` api.tp5.com/user/change_pwd

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_name|string|`必需`|无|用户名（手机/邮箱）|
|user_ini_pwd|string|`必需`|无|用户老密码|
|user_pwd|string|`必需`|无|用户新密码|

```javascript
{
    "code": 200,
    "msg": "密码修改成功！",
    "date": []
}
```

# 6. 用户找回密码
> `post` api.tp5.com/user/find_pwd

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_name|string|`必需`|无|用户名（手机/邮箱）|
|code|int|`必需`|无|验证码|
|user_pwd|string|`必需`|无|用户新密码|

```javascript
{
    "code": 200,
    "msg": "密码修改成功！",
    "date": []
}
```

# 7. 用户绑定手机号
> `post` api.tp5.com/user/bind_phone

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|code|int|`必需`|无|验证码|
|phone|string|`必需`|无|用户手机号|

```javascript
{
    "code": 200,
    "msg": "手机号绑定成功！",
    "date": []
}
```

# 8. 用户绑定邮箱
> `post` api.tp5.com/user/bind_phone

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|code|int|`必需`|无|验证码|
|email|string|`必需`|无|用户邮箱|

```javascript
{
    "code": 200,
    "msg": "邮箱绑定成功！",
    "date": []
}
```

# 9. 用户绑定手机/邮箱
> `post` api.tp5.com/user/bind_username

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|code|int|`必需`|无|验证码|
|user_name|string|`必需`|无|用户手机/邮箱|

```javascript
{
    "code": 200,
    "msg": "绑定成功！",
    "date": []
}
```

# 11. 用户修改昵称
> `post` api.tp5.com/user/nickname

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|user_nickname|string|`必需`|无|用户昵称|

```javascript
{
    "code": 200,
    "msg": "昵称修改成功！",
    "date": []
}
```

# 12. 新增文章
> `post` api.tp5.com/article

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|article_uid|int|`必需`|无|文章用户id|
|article_title|string|`必需`|无|文章标题|
|article_content|string|可选|无|文章内容|

```javascript
{
    "code": 200,
    "msg": "新增文章成功！",
    "date": []
}
```

# 13. 查看文章列表
> `get` api.tp5.com/articles

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|user_id|int|`必需`|无|用户id|
|num|int|可选|10|每页个数|
|page|int|可选|1|页码|

```javascript
{
    "code": 200,
    "msg": "查询成功！",
    "date": {
        "articles": [
            {
                "article_id": 2,
                "article_ctime": 1519960713,
                "article_title": "标题3",
                "user_nickname": null
            },
            {
                "article_id": 3,
                "article_ctime": 1519960751,
                "article_title": "标题3",
                "user_nickname": null
            }
        ],
        "page_num": 1
    }
}
```

# 14. 单个文章的详细信息
> `get` api.tp5.com/article

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|article_id|int|`必需`|无|文章id|

```javascript
{
    "code": 200,
    "msg": "查询成功",
    "date": {
        "article_id": 27,
        "article_title": "标题3",
        "article_content": "<script>adsfasd咋说</script>",
        "article_ctime": 1519972492,
        "user_nickname": "123123"
    }
}
```

# 15. 修改/保存文章
> `put` api.tp5.com/article

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|article_id|int|`必需`|无|文章id|
|article_title|string|`可选`|无|文章题目|
|article_content|string|`可选`|无|文章内容|

```javascript
{
    "code": 200,
    "msg": "文章更新成功！",
    "date": []
}
```

# 16. 删除文章
> `delect` api.tp5.com/article

|参数|类型|必需/可选|默认|描述
|:-|:-|:-|:-|:-|
|time|int|`必需`|无|`时间戳`用于判断请求是否超时|
|token|string|`必需`|无|确定来访者身份|
|article_id|int|`必需`|无|文章id|

```javascript
{
    "code": 200,
    "msg": "文章删除成功！",
    "date": []
}
```