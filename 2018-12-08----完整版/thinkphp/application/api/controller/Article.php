<?php
namespace app\api\controller;

class Article extends Common
{
  /**
   * 新增文章
   */
  public function add_article()
  {
    /***********  接受参数  ***********/
    $data = $this->params;
    $data['article_ctime'] = time();
    /***********  存入数据  ***********/
    $res = db('article')->insertGetId($data);
    if ($res) {
      $this->return_msg(200, '新增文章成功！', $res);
    } else {
      $this->return_msg(400, '新增文章失败！');
    }
  }
  /**
   * 查询文章列表[分页]
   */
  public function article_list()
  {
    /***********  接受参数  ***********/
    $data = $this->params;
    if (empty($data['num'])) {
      $data['num'] = 10;
    }
    if (empty($data['page'])) {
      $data['page'] = 1;
    }
    /***********  查询数据库  ***********/
    $where['article_uid'] = $data['user_id'];
    $where['article_isdel'] = 0;
    $count = db('article')->where($where)->count();
    $page_num = ceil($count / $data['num']); //总页数,向上取整
    $fileld = 'article_id,article_ctime,article_title,user_nickname'; //需要的字段
    $join = [['api_user u', 'u.user_id = a.article_uid']]; //连接查询，有几个表就包含几个元素，本身二维数组不变
    $res = db('article')->alias('a')->field($fileld)->join($join)->where($where)->page($data['page'], $data['num'])->select();
    /***********  判断并输出  ***********/
    if ($res === false) {
      $this->return_msg(400, '查询失败！');
    } elseif (empty($res)) {
      $this->return_msg(200, '暂无数据！');
    } else {
      $return_data['articles'] = $res;
      $return_data['page_num'] = $page_num;
      $this->return_msg(200, '查询成功！', $return_data);
    }
  }
  /**
   * 获取单个文章的详细信息
   */
  public function article_detail()
  {
    /***********  接受参数  ***********/
    $data = $this->params;
    /***********  查询数据库  ***********/
    $field = 'article_id,article_title,article_content,article_ctime,user_nickname';
    $where['article_id'] = $data['article_id'];
    $join = [['api_user u', 'u.user_id = a.article_uid']];
    $res = db('article')->alias('a')->field($field)->join($join)->where($where)->find();
    $res['article_content'] = htmlspecialchars_decode($res['article_content']);
    /***********  验证结果  ***********/
    if (!$res) {
      $this->return_msg(400, '查询失败');
    } else {
      $this->return_msg(200, '查询成功', $res);
    }
  }
  /**
   * 修改数据
   */
  public function update_article()
  {
    /***********  接受参数  ***********/
    $data = $this->params;
    /***********  存入数据  ***********/
    $res = db('article')->where('article_id', $data['article_id'])->update($data);
    /***********  验证结果  ***********/
    if ($res !== false) {
      $this->return_msg(200, '修改文章成功！');
    } else {
      $this->return_msg(400, '修改文章失败！');
    }
  }
  /**
   * 删除文章记录
   */
  public function del_article()
  {
    echo 'a';die;
    /***********  接受参数  ***********/
    $data = $this->params;
    /***********  逻辑删除（只是隐藏，可以恢复）  ***********/
    $res = db('article')->where('article_id', $data['article_id'])->setField('article_isdel', 1);
    /***********  物理删除（永久删除）  ***********/
        // $res = db('article')->delete($data['article_id']);
    /***********  验证结果  ***********/
    if ($res) {
      $this->return_msg(200, '删除文章成功！');
    } else {
      $this->return_msg(400, '删除文章失败！');
    }
  }
}