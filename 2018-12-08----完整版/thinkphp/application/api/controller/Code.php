<?php
namespace app\api\controller;

use phpmailer\phpmailer; //邮箱发送验证码类

class Code extends Common
{
  public function get_code()
  {
    $username = $this->params['username'];
    $exist = $this->params['is_exist'];
    /*********** 检验用户名的函数（在common中） ***********/
    $username_type = $this->check_username($username);
    switch ($username_type) {
      case 'phone':
        $this->get_code_by_username($username, 'phone', $exist);
        break;
      case 'email':
        $this->get_code_by_username($username, 'email', $exist);
        break;
    }
  }
  /**
   * 通过手机/邮箱获取验证码
   * @param string $username 手机号/邮箱
   * @param string $type 手机号/邮箱
   * @param int $exist 手机号/邮箱是否应该存在于数据库中 1：是，0：否
   * @return json API返回的json数据
   */
  public function get_code_by_username($username, $type, $exist)
  {
    if ($type == '$phone') {
      $type_name = '手机';
    } else {
      $type_name = '邮箱';
    }
    /***********  检测手机号是否存在  ***********/
    $this->check_exist($username, $type, $exist);
    /***********  检测验证码请求频率  30s一次  ***********/
    if (session("?" . $username . '_last_send_time')) {
      if (time() - session($username . '_last_send_time') < 30) {
        $this->return_msg(400, $type_name . '验证码，每30s只能发送一次！');
      }
    }
    /***********  生成验证码  ***********/
    $code = $this->make_code(6); //生成6位验证码
    /***********  使用session储存验证码，方便比对，md5加密  ***********/
        // $md5_phone = md5($username . '_' . md5($code));
    session($username . '_code', $code);
    /***********  使用session储存验证码的发送时间  ***********/
    session($username . '_last_send_time', time());
    /***********  发送验证码  ***********/
    if ($type == 'phone') {
      $this->send_code_to_phone($username, $code);
    } else {
      $this->send_code_to_email($username, $code);
    }
  }
  /**
   * 生成验证码
   * @param int $num 验证码的位数
   * @return int 生成的验证码
   */
  public function make_code($num)
  {
    $max = pow(10, $num) - 1;
    $min = pow(10, $num - 1);
    return rand($min, $max);
  }
  /**
   * 向邮箱发送验证码
   * @param string $email 目标email
   * @param int $code 验证码
   * @return json 返回的数据
   */
  public function send_code_to_email($email, $code)
  {
    $toemail = $email;
    $mail = new PHPMailer();
    $mail->isSMTP(); //开启SMTP服务
    $mail->CharSet = 'utf-8';
    $mail->Host = 'smtp.126.com';
    $mail->SMTPAuth = true; //是否需要验证身份
    $mail->Username = 'zhangyulongzzzz@126.com'; //发送方的邮箱，我们自己的邮箱
    $mail->Password = '295993090zzyyll'; //邮箱密码
    $mail->SMTPSecure = 'ssl'; //使用ssl协议
    $mail->Port = 994; //ssl协议端口994
    $mail->setFrom('zhangyulongzzzz@126.com', '接口测试'); //完善发送房信息
    $mail->addAddress($toemail, 'test'); //接受方,向多人发送，添加多行此语句
    $mail->addReplyTo('zhangyulong@126.com', 'Reply'); //回复人信息
    $mail->Subject = '您有新的验证码'; //邮件的标题
    $mail->Body = "这是一个测试邮件，您的验证码是$code,验证码的有效期为1分钟，本邮件请勿回复！"; //邮件内容
    if (!$mail->send()) {
      $this->return_msg(400, $mail->ErrorInfo);
    } else {
      $this->return_msg(200, '验证码已经发送请注意查收');
    }
  }
  public function send_code_to_phone()
  {
    echo 'send_code_to_phone';
  }
}