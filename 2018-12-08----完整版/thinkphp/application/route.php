<?php

use think\Route;

/*********** 基础 ***********/
// api.tp5.com ===> www.tp5.com/index.php/api
Route::domain('api', 'api');

/*********** 验证码 ***********/
// post api.tp5.com ===> user.php login()
Route::post('user', 'user/login');
// 获取验证码
Route::get('code/:time/:token/:username/:is_exist', 'code/get_code');

/*********** user  用户 ***********/
// 用户注册
Route::post('user/register', 'user/register');
// 用户登录
Route::post('user/login', 'user/login');
// 用户上传头像
Route::post('user/icon', 'user/upload_head_img');
// 用户修改密码
Route::post('user/change_pwd', 'user/change_pwd');
// 用户找回密码
Route::post('user/find_pwd', 'user/find_pwd');
// 用户绑定手机号
Route::post('user/bind_phone', 'user/bind_phone');
// 用户绑定邮箱
Route::post('user/bind_email', 'user/bind_email');
// 用户绑定手机、邮箱
Route::post('user/bind_username', 'user/bind_username');
// 用户修改昵称
Route::post('user/nickname', 'user/set_nickname');

/*********** article 文章 ***********/
// 新增文章
Route::post('article', 'article/add_article');
// 获取文章列表(time/token/用户ID/显示多少条、可选/显示第几页、可选)
Route::get('articles/:time/:token/:user_id/[:num]/[:page]', 'article/article_list');
// 获取单个文章详细信息
Route::get('article/:time/:token/:article_id', 'article/article_detail');
// 修改/更新文章
Route::put('article', 'article/update_article');
// 删除文章
Route::delete('article/:time/:token/:article_id', 'article/del_article');
// Route::post('article/:time/:token/:article_id', 'article/del_article');
// Route::delete()